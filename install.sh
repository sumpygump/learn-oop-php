#!/bin/bash

# This script will run composer install
# The composer json file defines some post install hooks to set up the project

composerbin=$(which composer.phar)
rc=$?
if [[ $rc != 0 ]] ; then
    composerbin=$(which composer)
    rc=$?
    if [[ $rc != 0 ]] ; then
        echo "Cannot find composer. Make sure it is on your path. Exiting."
        exit 1
    fi
fi

echo $composerbin install
$composerbin install

echo Done.
