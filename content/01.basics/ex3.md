---
title = Class Methods
exercise = 3
---

# Exercise 3: Class Methods

## What an Object Does

We learned that a property holds the data of an object, which consists of what
the object *knows*. A method provides us with what the object can *do*. Methods
and properties are collectively called *members* of an object or class.

A method of a class is the same exact thing as a function in procedural PHP
code, so you are already familiar with the construct. The only thing to be
aware of is that a method can have keywords in front of it, for example, the
`public` keyword we have already seen.

An example of a class with some defined methods:

```
class BlogPost
{
    public $title = '';

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}
```

A common practice is to create *getters and setters* for object properties to
set and retrieve their values. The reason why you would want to do this is so
you can add logic to the setting and retrieval of an object's data.

```
$blogPost = new BlogPost();
$blogPost->setTitle("This is my fancy post");

print $blogPost->getTitle();
```

## Strong and Clear Names

With methods you are creating a powerful system of how you want users to
interact with your objects. It is through the object's methods we act and
accomplish the work of the object. The outside world doesn't need to know
exactly what goes on in each method, but it should be clear and concise what
they are able to do by the exposed methods.

Your method names should clearly define what is going to happen, so when I use
your object I have a good idea of which ones I want to use to accomplish the
tasks I want to complete. It is a good idea to start each method name with a
verb. Here are some examples of method names taken from real projects:

 - filter()
 - sort()
 - getId()
 - regenerate()
 - loadSession()
 - open()
 - describeInputDefinition()
 - enableHttpMethodParameterOverride()
 - writeData()
 - initialize()
 - execute()

<div class="callout callout_convention">
<div class="callout-body" markdown="1">
A class method should be short and perform one task. When a method is too long,
it should be split up into smaller methods that are easier to read and perform
more granular actions of the task.
</div>
</div>

## Extra Credit

1. Write the class from exercise 2 but add getters and setters for each
   property defined in that class.
2. What happens when you attempt to invoke a method on a class that doesn't
   exist? Example: `$blogPost->methodThatDoesntExist();`
3. Think about some old code you have written. If it has no classes, think
   about what classes you would make. What properties would you define for
   each class? What method names would you define?
