---
title = Class
exercise = 1
---

# Exercise 1: Class

## What is a Class?

A *class* is a definition of a blueprint or recipe for creating an object.

Making an object from a class is called *instantiation*. You can make many
objects from a class.

A class is defined by the keyword `class` and must have a name that can only
contain numbers, letters and `_` (underscore). A class name cannot start with a
number though.

A class is defined at a bare minimum as follows:

```
class BlogPost
{
}
```

An object from this class can be created using the following example code:

```
$myBlogPost = new BlogPost();
```

<div class="callout callout_convention">
<div class="callout-body" markdown="1">
It's a common practice to name classes starting with an **uppercase letter and
in camel case**. That is why you'll see class names like `BlogPost` and not
`blog_post`. The filename should reflect the class name, so the class
`BlogPost` should be defined in a file named *BlogPost.php*.
</div>
</div>

## Constructor

The constructor is a method on a class that gets called when a new object is
created from that class. In PHP, the constructor is a method called
`__construct`. The arguments passed into the constructor are the arguments
given when creating the object with the `new` keyword. The constructor method
should not have a `return` statement, because it will always return an object
of the given class.

Example:

```
class BlogPost
{
    public function __construct($title, $body)
    {
        // I can do things with $title and $body
    }
}

$myBlogPost = new BlogPost('a title', 'a body');
```

In the above example, the constructor (`__construct`) is called with the line
`$myBlogPost = new BlogPost('a title', 'a body');`. Inside this method,
*$title* will be 'a title' and *$body* will be 'a body'.

## Extra Credit

1. When I said a class name must consist only of numbers, letters or
   underscore, I wasn't 100% accurate. What is the exact rule for a class name
   in PHP?
2. In PHP version 4, the name of the constructor method was different. How were
   constructors named in PHP 4?
3. Define a class with and without a constructor. Try to instantiate an object
   with providing arguments and not. What happens?
