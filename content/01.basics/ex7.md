---
title = Interface
exercise = 7
---

# Exercise 7: Interface

Now we're getting into some fun stuff. An *interface* is a contract for a class.
From the PHP documentation:

> Object interfaces allow you to create code which specifies which methods a
> class must implement, without having to define how these methods are handled.

With an interface we have some defined methods that will be guaranteed to be
available. That way when our code interacts with classes who *implement* that
interface we know that we can use it in a certain way.

## An Example: The Countable Interface

Let's take a look at some built in interfaces that PHP provides. One of PHP's
interfaces is called [`Countable`](http://us1.php.net/manual/en/class.countable.php).

The countable interface requires that a class has a method `count()`. By
creating a class that implements the Countable interface, we are able to count
that object.

For this example, we are going to introduce a BlogPostCollection, which is a
collection of blog posts. As such it will contain 0 or more blog posts and we
should be able to count how many it currently holds.

```php
class BlogPostCollection implements Countable
{
    protected $blogPosts = array();

    public function count()
    {
        return count($this->blogPosts);
    }

    public function addBlogPost($blogPost)
    {
        $this->blogPosts[] = $blogPost;
    }
}
```

Above is the definition of our BlogPostCollection that implements the Countable
interface. You can see in order to describe your class as adhering to the
interface, use the keyword `implements` and then the name of the interface.

Since the Countable interface requires the `count()` method we just have to
make sure that we at very least include that method.

So now here is how we can use that collection object:

```php
// Make our collection object
$blogPostCollection = new BlogPostCollection();

// Make a blogpost object so we can add it to the collection.
$blogPost = new BlogPost();

$blogPostCollection->addBlogPost($blogPost);

echo count($blogPostCollection);
```

The magic of the this particular interface is that we can use it as an argument
of PHP's `count()` function, because it accepts things that can be counted.
Arrays, and objects that implement the Countable interface. Internally what
happens is that when `count()` is called on a Countable object, it uses the
objects `count()` method because it knows it can rely on it being there because
it implements the Countable interface.

## Defining an Interface

In order to define our own interface it looks like a class definition but
instead we use the word *interface* and define methods without any body.

```php
interface ViewRendererInterface
{
    public function setLayout($layout);
    public function setTemplate($template);
    public function render($content);
}
```

In this example we are defining an interface that we will use to render a view.
It requires three methods (`setLayout`, `setTemplate`, `render`). We don't care
about the implementation of how it is accomplished but we require to be able to
use a class that implements this interface by using those methods.

<div class="callout callout_convention">
<div class="callout-body" markdown="1">
It's common practice to have some indication in the name of your interfaces
that is is an interface. Sometimes the name is prefixed with an `I`, for
example `ISessionHandler`. Sometimes it ends with the word Interface, for
example, `SessionHandlerInterface`. These conventions are used during
development to help differentiate between classes and interfaces.
</div>
</div>

Now we can create a class that implements our ViewRendererInterface.

```php
class HtmlViewRenderer implements ViewRendererInterface
{
    protected $layout;
    protected $template;

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function setTemplate($template)
    {
        $this->template = $template;
    }

    public function render($content)
    {
        // Do some work with the layout and template probably
        return $content;
    }
}
```

With this example, anywhere in our code where we require a
*ViewRendererInterface* we know that the *HtmlViewRenderer* will work because
it implements that interface.

## Multiple Interfaces

You can create a class that implements more than one interface:

```php
class BlogPostCollection implements Countable, ArrayAccess, Traversable,
Iterator
{
    // Methods that fulfill all specified interfaces
}
```

## Extra Credit

1. Find out what happens when you try to create a class that implements an
   interface but forget to make one or more of the required methods for that
   interface.
2. Find out what other predefined interfaces are available in PHP. Try
   implementing one of them.
3. What does the PHP function `get_declared_interfaces` do?
