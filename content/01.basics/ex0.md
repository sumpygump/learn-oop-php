---
title = Exercise 0
exercise = 0
---
# Exercise 0: Object

## What is an Object?

An object is a collection of *properties* and *methods*. It's properties are
what it knows and its methods are what it can do. In most cases an object
should represent a real-world object, or a programming conceptual object. To
create a new object (which is an instance of a class) use the `new` keyword.
Interact with objects by calling methods and referencing properties on them
using the arrow symbol (`->`).

Below is some example code illustrating creation and usage of an object:

```php
$blogPost = new BlogPost('title', 'body');
$blogPost->setAuthor('Jansen Price');
$blogPost->addTag('coding');
$blogPost->setIsPublished(true);

// Render
$blogPostRenderer = new BlogPostRenderer();
echo $blogPostRenderer->renderPost($blogPost);
```

## Creating an Object

To create a new object, use the keyword `new`, followed by a *class name* and
some arguments to pass to the class's [*constructor*](oo-terms#constructor).
The *class name* is a template for creating an instance of an object of that
class. In strongly typed languages, like C#, a class name is also considered
its *type*, but in PHP, the type of the variable that is an instance of an
object is 'object.'

```php
$blogPost = new BlogPost('title', 'body');
```

In this code example, we are creating a new object from the class `BlogPost`
and assigning it to the variable `$blogPost`. The arguments 'title' and 'body'
are being passed as the BlogPost's first and second arguments, respectively.

The constructor is a method that is called when an object is created and can
take 0 or more arguments, just like a function. In this example, to create a
BlogPost object one can supply two arguments, a title for the blog post and the
body of the blog post.

You can have as many instances of an object as you want (well, until your
computer's memory's limit is reached).

```php
$blogPost = new BlogPost('About my trip to the Bahamas', 'Last month I got out
of this stinking town and got on a plane...');

$anotherBlogPost = new BlogPost('How I got food poisoning', "I will never eat
at Bob's burger shack again...");
```

## Getting Information About an Object

PHP's function `get_class` will return the class name of an object. This is
useful if you need to know exactly what class of an object you may be dealing
with.

```php
echo get_class($blogPost);  // Will output 'BlogPost'
```

Related functions are `gettype`, `is_object`, and the operator `instanceof`.

```php
echo gettype($blogPost); // 'object'
echo is_object($blogPost); // 1 (meaning true)
echo $blogPost instanceof BlogPost; // 1 (meaning true)
```

## Extra Credit

 1. Find out what PHP's *StdClass* is. Create an object from a *StdClass* and then call
   `get_class()` and `gettype()` on it. What is the output?
 2. What happens when I try to create an object from a class that doesn't exist?
   Example: `$myObject = new ClassThatDoesntExist();`
 3. What happens when I attempt to access a property that doesn't exist on an
    object? Example `echo $myObject->aValueThatDoesntExist;`
