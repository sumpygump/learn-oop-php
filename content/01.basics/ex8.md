---
title = Inheritance
exercise = 8
---

# Exercise 8: Inheritance

A simple way of describing inheritance is:

> The ability of creating a new class from an existing class.

## A Simple Example

In this example, we have a `Person` class that represents a person. It has a
name and it can talk. A doctor is very similar to a person, so rather than
redefine a class with the same `name` property and `talk()` method, we can
create a `Doctor` class that *extends* the `Person` class. It will include the
same things as a `Person`, but we can change its behavior.

![Person is a superclass of Doctor](assets/inheritance-person-doctor.png)

<div class="callout callout_takenote">
<div class="callout-body" markdown="1">
In case you haven't seen it before, this is called a *UML class diagram*. It is
used to describe the relationship between classes an objects in a software
model. In this diagram it indicates that the class *Doctor* extends the class
*Person*.
</div>
</div>

```php
class Person
{
    protected $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function talk
    {
        echo 'Hello';
    }
}

class Doctor extends Person
{
    public function talk
    {
        echo 'Mylanta';
    }
}
```

The `Doctor` class is considered a *child* class because it extends `Person`. The
`Person` class is a *parent* class or *superclass*.

Here is how those two classes can be used:

```php
$robert = new Person();
$robert->setName('Robert');
echo $robert->talk(); // "hello"

$drRichards = new Doctor();
$drRichards->setName('Dr. Richards');
echo $drRichards->talk(); // "mylanta"
```

When we call `talk()` on the person object, we get "hello". When we call
`talk()` on the doctor object, we get "mylanta" ("My doctor said 'Mylanta'").

## A More Complex Example

Let's take a look at Symfony's ParameterBag class. It has two child classes:
ServerBag and FileBag. A parameter bag is a holder of parameters. For example,
we could use it store everything from `$_GET` or `$_POST`. We also require a
FileBag which is very similar to a ParameterBag but it needs to have some
additional functionality to handle File parameters. Similarly, the ServerBag
class is almost identical to ParameterBag, but it has an additional method
`getHeaders`.

![ParameterBag Children](assets/parameterbag-children.png)

```php
class ParameterBag
{
    protected $parameters;

    public function __construct($parameters = array())
    {
        $this->parameters = $parameters;
    }

    public function all()
    {
        return $this->parameters;
    }
    
    public function replace($parameters = array())
    {
        $this->parameters = $parameters;
    }

    public function set($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    // ... etc.
}
```

```php
class FileBag extends ParameterBag
{
    public function __construct($parameters = array())
    {
        $this->replace($parameters);
    }

    public function replace($files = array())
    {
        $this->parameters = array();
        $this->add($files);
    }

    public function add($files = array())
    {
        foreach ($files as $key => $file) {
            $this->set($key, $file);
        }
    }

    public function set($key, $value)
    {
        if (!is_array($value) && !$value instanceof UploadedFile) {
            throw new \InvalidArgumentException('An uploaded file must be an array or an instance of UploadedFile.');
        }

        parent::set($key, $this->convertFileInformation($value));
    }

    // ... etc.
}
```

```php
class ServerBag extends ParameterBag
{
    public function getHeaders()
    {
        // logic to return headers...
    }
}
```

Note that in this example, the following methods were changed in the `FileBag`
class: `__construct()`, `set()` and `replace()`.

Also notice that in the `set()` method of the `FileBag` class you will see the
word `parent` being used. This is how you invoke or access something from the
parent class from the child. Notice that you use the double colon just as when
you use `self`.

## Extra Credit

1. Create a class and a child class as practice. Have the objects represent
   real world entities.
2. Look at the PHP function [`get_parent_class`](http://us3.php.net/manual/en/function.get-parent-class.php) and find out what it does. Try it out on your object.
3. Look at the PHP function [`is_a`](http://us3.php.net/manual/en/function.is-a.php) and find out what it does. Try it out on some objects.
