---
title = Class Constants
exercise = 4
---

# Exercise 4: Class Constants

## Defining Class Constants

A constant for a class is similar to a global constant, created using the
`define` keyword. In a class, however, the constant is created using the
`const` keyword.

```
class BlogPost
{
    const STATUS_PUBLISHED = "published";
    const STATUS_UNPUBLISHED = "unpublished";

    const MAX_COMMENTS = 50;
}
```

Constants are useful to define unchanging data. By having a constant defined it
prevents typos of strings and numbers that may be used in the code. It also
assigns meaning to values that will be used throughout the code instead of
relying on everyone knowing what "code 16" is, for example.

<div class="callout callout_convention">
<div class="callout-body" markdown="1">
It's a common practice to name constants in all caps with underscores as a word separator. Example: `TYPE_POST`
</div>
</div>

## Accessing Class Constants

You refer to constants using the *Scope Resolution Operator* symbol (`::`),
also known as the double colon or Paamayim Nekudotayim (means double colon in
Hebrew). This is how you refer to static members on classes as well. We'll
learn about statics in a later exercise.

Syntax for referring to a class constant:

```
BlogPost::STATUS_UNPUBLISHED;
```

Note that you cannot refer to a constant from an instance of an object, only
using the class name.

```
$blogPost = new BlogPost();
$anotherBlogPost = new BlogPost();

$blogPost->setStatus(BlogPost::STATUS_PUBLISHED);
$anotherBlogPost->setStatus(BlogPost::STATUS_UNPUBLISHED);
```

## Using `self`

You can also refer to a class constant inside a class using the `self` keyword,
like so:

```
class BlogPost
{
    const STATUS_PUBLISHED = "published";
    const STATUS_UNPUBLISHED = "unpublished";

    public $status = self::STATUS_UNPUBLISHED;

    public function publish()
    {
        // This will mark this blog post as published
        $this->status = self::STATUS_PUBLISHED;
    }

    public function isPublished()
    {
        // This method will return whether the status is currently set to
        // published
        return $this->status == self::STATUS_PUBLISHED;
    }
}
```

See also the page for [Class Constants at PHP.net](http://www.php.net/manual/en/language.oop5.constants.php)

## Extra Credit

1. What happens when you try to access a constant from an instance of the
   object instead of using the class name?
2. Look at some code of yours and identify any values where you could have used
   a constant instead of hard-coding a string or a number. What would you name
   the constant(s)?
