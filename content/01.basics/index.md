---
title = Basics
---

# Basics

Exercises 0 through 14 cover the basics of objects and classes.

 - [Exercise 0: What is an Object](basics/ex0)
 - [Exercise 1: Defining a Class, Constructors](basics/ex1)
 - [Exercise 2: Class Properties](basics/ex2)
 - [Exercise 3: Class Methods](basics/ex3)
 - [Exercise 4: Class Constants](basics/ex4)
 - [Exercise 5: Visibility](basics/ex5)
 - [Exercise 6: Static Keyword](basics/ex6)
 - [Exercise 7: Interface](basics/ex7)
 - [Exercise 8: Inheritance](basics/ex8)
 - [Exercise 9: Abstract](basics/ex9)
 - [Exercise 10: Type Hinting](basics/ex10)
 - [Exercise 11: Magic Methods](basics/ex11)
 - [Exercise 12: Namespaces](basics/ex12)
 - [Exercise 13: Traits](basics/ex13)
 - [Exercise 14: Autoloading](basics/ex14)
