---
title = Visibility
exercise = 5
---

# Exercise 5: Visibility

There are three keywords that we can use for class members that define their
visibility. We have already seen one: the *public* keyword.

## Public

The *public* keyword is applied to class members (methods and properties) and
defines them as visible and changable by code outside of the object.

```
class BlogPost
{
    public $title = '';

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}
```

Having class members public is like having a hole in your wall in your living
room. You might say to yourself it is for convenience so that anyone who walks
by can watch the TV. It also means that someone can walk by and replace your
TV with a banana peel or something. Not cool.

```
$blogPost = new BlogPost("My title");
$blogPost->title = "My new title!";
echo "The title of the post is '" . $blogPost->title . "'";

$blogPost->title = array("sorry about your title");
echo "The title of the post is '" . $blogPost->title . "'";
```

In this code example we have allowed the title to be a public property and then
we set it to an array, which means now we don't know how to properly display it
at any given moment. When you expose the data of your object you cause outside
entities to have too much control and to worry too much about your object.

## Private

To prevent outside abuse, we can apply the *private* keyword instead.

```
class BlogPost
{
    private $title = '';

    public function __construct($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }
}
```

In this example, we have set the `$title` property to be *private*. As a private
member, it is invisible and inaccessible to any code outside the object.

```
$blogPost = new BlogPost("This is my title");
echo $blogPost->title; // This will fail, title is invisible outside the object.
```

Because the title property is invisible, it is not accessible and so that is
why we would add a *getter* method that is publicly accessible.

What visibility allows you to do is to hide the details of how your object
works and expose methods to the outside world to perform the tasks important to
the object. This is one of the hallmarks of OOP, since others need not be
burdened with the details of how data is stored or how computations occur
within your object. This way, you can change how it works without breaking
anything as long as your publicly accessible parts remain consistent.

For example, maybe we store the data in this manner:

```
class BlogPost
{
    private $data = array(
        'title' => '',
        'body' => '',
        'author' => '',
        'postDate' => '',
    );

    public function __construct($title, $body)
    {
        $this->setTitle($title);
        $this->setBody($body);
    }

    public function setTitle($title)
    {
        $this->data['title'] = $title;
    }

    public function getTitle()
    {
        return $this->data['title'];
    }

    public function setBody($body)
    {
        $this->data['body'] = $body;
    }

    public function getBody()
    {
        return $this->data['body'];
    }
}
```

In this code sample, we decided to store the actual data of the blog post in
a different manner, but this won't break anything as far as outside code is
concerned because it should have only been using the public methods
(`getTitle`, `getBody`).

## Protected

The third visibility keyword is *protected*. This is similar to private because
it is invisible to the outside world. But it differs from private because
private is also invisible to *child classes*. This is a concept from
*inheritance* which we'll learn about later. A protected member is accessible
by the class itself as well as any child classes (and objects).

```
class BlogPost
{
    protected $title = '';

    public function __construct($title)
    {
        $this->title = $title;
    }
}
```

## Methods Too

In this exercise we have only talked about applying the private and protected
keywords to properties. Keep in mind that you can also create *private* and
*protected* **methods** in your objects. This is good for breaking up internal
tasks that need to be accomplished by the object but which you don't want to
expose (burden) to the outside world.

<div class="callout callout_takenote">
<div class="callout-body" markdown="1">
I recommend to get in the habit of by default defining properties or methods
you don't need to be accessible to the outside world to be `protected`. This
will allow any inherited classes to change behavior if needed **unless** there
is a specific reason you don't want inherited classes to affect behavior (then
use private).
</div>
</div>

## Extra Credit

1. Rewrite any classes from past exercises using private or protected keywords.
