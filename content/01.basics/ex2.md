---
title = Class Properties
exercise = 2
---

# Exercise 2: Class Properties

## What an Object Knows

A *property* is a named variable representing data or state of an object.

Properties are defined on an object like so:

```php
class BlogPost
{
    public $title = '';
    public $body = '';
    public $author = 'Jansen Price';
    public $date = '';
    public $isPublished = false;
    public $tags = array();
    public $comments;
}
```

A property can be given a default value when it is defined as indicated with
the equals signs above. If it is not given a default value, it will be null.

Default values of properties can be strings, integers, booleans, arrays and
null. They cannot be expressions or objects. If you want to default a property
value to an object or a calculated value, you must set it in the constructor.

```
class BlogPost
{
    public $title = '';
    public $body = '';

    public function __construct($title, $body)
    {
        $this->title = $title;
        $this->body = $body;
    }
}
```

In this constructor, we are taking in the title and body arguments and setting
them to their respective properties. This is how we can allow the user to
supply the title and body of the blog post immediately when they create it. In
fact, the user *must* provide the title and the body when they create a new
object with how we have defined it here.

Inside the constructor (and other methods), `$this` is how you refer to the
current object.

## Accessing and Setting Properties

The arrow symbol (`->`) is how you refer to a member of an object, whether it
be a property or a method.

```php
$blogPost = new BlogPost("This is my fancy title", 'body');
echo $blogPost->title; // Results in "This is my fancy title"

echo $blogPost->date; // Empty string, no date has been set yet.
$blogPost->date = "January 2nd, 2014"; // Sets the date
echo $blogPost->date; // Results in "January 2nd, 2014"
```

## Extra Credit

1. Create a class that has some properties. Make some with default values and
   some without.
2. Try to set a property on an object that wasn't declared in the class
   definition. Then access that property (print it out). What happens?
3. What happens when you attempt to access a property that hasn't been defined
   or set on an object? Example: `print $myObject->propertyThatDoesntExist`
