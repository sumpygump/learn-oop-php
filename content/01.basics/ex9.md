---
title = Abstract
exercise = 9
---

# Exercise 9: Abstract

## The Abstract Class

We have learned that we can extend a class and the child class inherits all the
properties and methods from the parent.

```php
class View
{
    public function __construct()
    {
    }

    public function render()
    {
        // do something
    }
}

class HtmlView extends View
{
    public function render()
    {
        // do something
    }
}

class CliView extends View
{
    public function render()
    {
        // do something
    }
}
```

This means we can do the following:

```php
$myView = new View();
$myView->render();

$myHtmlView = new HtmlView();
$myHtmlView->render();
```

We may not want people to create an object from the superclass `View`. We may
deem that as not having enough meaning or not providing the proper
functionality.

It would be nice if we could prevent people from making objects from the `View`
class, and force the creation of child classes where the full logic can be
realized. This is what an *abstract* class is.

To create an abstract class, add the modifier `abstract` in the class
declaration:

```php
abstract class View
{
    public function __construct()
    {
        // logic
    }

    abstract public function render()
    {
        // do something
    }
}

class HtmlView extends View
{
    public function render()
    {
        // do something
    }
}

class CliView extends View
{
    public function render()
    {
        // do something
    }
}
```

Now with `View` being an abstract class, it is not possible to create an object
from the abstract class so we want the user to extend it to create their
version that can be created into objects.

```php
$myView = new View(); // fails

$myHtmlView = new HtmlView();
$myHtmlView->render();
```

Note that the method `render()` is also declared as abstract in the `View()`
abstract class. This means that a child class must create their own
implementation in order to use it.

## Extra Credit

1. What is the error message that PHP produces if you try to create an object
   from an abstract class?
