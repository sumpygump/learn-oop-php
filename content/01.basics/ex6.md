---
title = Static Keyword
exercise = 6
---

# Exercise 6: Static Keyword

We learned about the visibility keywords, *public*, *private* and *protected*.
There is another keyword that can be stacked on to members and that is
*static*. What static does is make the associated member tied to the class and
not the object. This means that it is available to every single instance of the
class (every single object created from that class).

Here is an example. In this example we're going to pretend we'll need an asset
path available to the blog post, but we don't want to have multiple copies of
the path hanging around, we want all blog post objects to share the exact same
path because it is common among all.

```
class BlogPost
{
    protected static $assetPath = "";

    public static function setAssetPath($assetPath)
    {
        self::$assetPath = $assetPath;
    }

    public function getAssetPath()
    {
        return self::$assetPath;
    }
}
```

Note that we must use the double colon (*scope resolution operator*) to access the static member
`$assetPath`.

You should note that it behaves like a constant but with a constant we cannot
change the value at run time but with a static we can.

This is how we set the static property:

```
BlogPost::setAssetPath(realpath('user-assets/'));
```

Note that this is a static method we call. We call it using the class name and
not an instance (an object), because the static lives at the class. Any object
of that class will have access to it by virtue of the fact that they are made
from that class.

This is why our non-static convenience method (`getAssetPath`) to get it from
an object works nicely:

```
$blogPostOne = new BlogPost();
$blogPostTwo = new BlogPost();

echo $blogPostOne->getAssetPath();
```

## Extra Credit

1. What happens when you try to access a static member as if it was an instance
   member?  Example: `$blogPost->assetPath`
2. Read additional information about the [static keyword at PHP.net](http://us1.php.net/manual/en/language.oop5.static.php)
