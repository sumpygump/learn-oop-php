---
title = Principles
---

# Principles

These exercises cover more abstract concepts of OO programming including
principles and best practices.

 - [Exercise 15: Abstraction, Encapsulation, Inheritance, Polymorphism](principles/ex15)
 - [Exercise 16: Composition](principles/ex16)
 - [Exercise 17: Single Responsibility Principle (SRP)](ex17)
 - [Exercise 18: The Open/Closed Principle (OCP)](ex18)
 - [Exercise 19: Liskov Substitution Principle (LSP)](ex19)
 - [Exercise 20: Interface Segregation Principle (ISP)](ex20)
 - [Exercise 21: Dependency Inversion Principle (DIP)](ex21)
 - [Exercise 22: High Cohesion, Low Coupling](ex22)
 - [Exercise 23: Law of Demeter (LoD)](ex23)
 - [Exercise 24: Inappropriate Intimacy](ex24)
 - [Exercise 25: The Law of Demeter](ex25)
 - [Exercise 26: Tell, Don't Ask](ex26)
 - [Exercise 27: Say It Once](ex27)
 - [Exercise 28: Don't Repeat Yourself (DRY)](ex28)

