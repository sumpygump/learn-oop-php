---
title = Object Oriented Terms
---

# Terms

## Constructor

The constructor is a method of a class that is invoked when an object of that
class is created. It is named `__construct`. See [exercise 1](ex1#constructor).
