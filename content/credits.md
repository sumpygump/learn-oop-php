
<p>Icon credits:</p> 
 - *Solution* by TNS from The Noun Project
 - *Reminder* designed by Ryan Sun from the Noun Project
 - *Alert* designed by Istiko Rahadi from the Noun Project
 - *Light Bulb* designed by Jean-Philippe Cabaroc from the Noun Project.
