---
title = Welcome
---

# Learn Object Oriented Programming for PHP

This is a series of exercises to introduce you to object oriented programming
for PHP. I am assuming you are an experienced programmer in PHP and are
familiar with procedural programming concepts.

## Basics

Exercises 0 through 14 cover the basics of objects and classes.

 - [Exercise 0: What is an Object](basics/ex0)
 - [Exercise 1: Defining a Class, Constructors](basics/ex1)
 - [Exercise 2: Class Properties](basics/ex2)
 - [Exercise 3: Class Methods](basics/ex3)
 - [Exercise 4: Class Constants](basics/ex4)
 - [Exercise 5: Visibility](basics/ex5)
 - [Exercise 6: Static Keyword](basics/ex6)
 - [Exercise 7: Interface](basics/ex7)
 - [Exercise 8: Inheritance](basics/ex8)
 - [Exercise 9: Abstract](basics/ex9)
 - [Exercise 10: Type Hinting](basics/ex10)
 - [Exercise 11: Magic Methods](basics/ex11)
 - [Exercise 12: Namespaces](basics/ex12)
 - [Exercise 13: Traits](basics/ex13)
 - [Exercise 14: Autoloading](basics/ex14)

## Principles

These exercises cover more abstract concepts of OO programming including
principles and best practices.

 - [Exercise 15: Abstraction, Encapsulation, Inheritance, Polymorphism](principles/ex15)
 - [Exercise 16: Composition](principles/ex16)
 - [Exercise 17: Single Responsibility Principle (SRP)](ex17)
 - [Exercise 18: The Open/Closed Principle (OCP)](ex18)
 - [Exercise 19: Liskov Substitution Principle (LSP)](ex19)
 - [Exercise 20: Interface Segregation Principle (ISP)](ex20)
 - [Exercise 21: Dependency Inversion Principle (DIP)](ex21)
 - [Exercise 22: High Cohesion, Low Coupling](ex22)
 - [Exercise 23: Law of Demeter (LoD)](ex23)
 - [Exercise 24: Inappropriate Intimacy](ex24)
 - [Exercise 25: The Law of Demeter](ex25)
 - [Exercise 26: Tell, Don't Ask](ex26)
 - [Exercise 27: Say It Once](ex27)
 - [Exercise 28: Don't Repeat Yourself (DRY)](ex28)

## Patterns

These exercises cover OO design patterns

 - [Singleton](404)
 - [Adapter](404)
 - [Lazy Initialization](404)
 - [Template](404)
 - [Factory](404)
 - [Abstract Factory](404)
 - [Strategy](404)
 - [Composite](404)
 - [Decorator](404)
 - [Builder](404)
 - [Chain of Responsibility](404)
 - [Iterator](404)
 - [Observer](404)
 - [Null Object](404)
 - [Object Pool](404)
