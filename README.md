Learn Object Oriented Programming in PHP
========================================

This is a series of exercises for learning OO in PHP for a developer who has
experience in PHP programming but wants to learn more about OO.

This is loosely based on the "learn python the hard way" and other books.

This is created using "greengrape" http://github.com/sumpygump/greengrape
